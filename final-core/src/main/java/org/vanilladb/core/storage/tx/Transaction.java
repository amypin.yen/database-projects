/*******************************************************************************
 * Copyright 2017 vanilladb.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.vanilladb.core.storage.tx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.vanilladb.core.server.VanillaDb;
import org.vanilladb.core.sql.Constant;
import org.vanilladb.core.storage.buffer.BufferMgr;
import org.vanilladb.core.storage.file.BlockId;
import org.vanilladb.core.storage.metadata.TableInfo;
import org.vanilladb.core.storage.record.RecordPage;
import org.vanilladb.core.storage.tx.concurrency.ConcurrencyMgr;
import org.vanilladb.core.storage.tx.concurrency.LockAbortException;
import org.vanilladb.core.storage.tx.concurrency.ReadInfo;
import org.vanilladb.core.storage.tx.concurrency.UndoBuffer;
import org.vanilladb.core.storage.tx.concurrency.UndoBufferMgr;
import org.vanilladb.core.storage.tx.recovery.RecoveryMgr;

/**
 * Provides transaction management for clients, ensuring that all transactions
 * are recoverable, and in general satisfy the ACID properties with specified
 * isolation level.
 */
public class Transaction {
	private static Logger logger = Logger.getLogger(Transaction.class.getName());

	private RecoveryMgr recoveryMgr;
	private ConcurrencyMgr concurMgr;
	private BufferMgr bufferMgr;
	private UndoBufferMgr undoBufferMgr;
	private List<TransactionLifecycleListener> lifecycleListeners;
	private long txNum;
	private boolean readOnly;
	private ArrayList<UndoBufferInfo> undoBufferList = new ArrayList<UndoBufferInfo>();
	private long startTime;
	private Map<BlockId, Vector<SlotNumChain> > insertedMap = new HashMap<BlockId, Vector<SlotNumChain>>();
	private Map<BlockId,ArrayList<ReadsetInfo>> readSet = new HashMap<BlockId,ArrayList<ReadsetInfo>>();
	private boolean hadDelete;
	private Map<BlockId, Vector<SlotNumChain> > deleteMap = new HashMap<BlockId, Vector<SlotNumChain>>();
	
	class UndoBufferInfo{
		BlockId blk;
		int slotNum;
		
		UndoBuffer undobuff;
		
		UndoBufferInfo(BlockId blk, int slotNum, UndoBuffer buff){
			this.blk = blk;
			this.slotNum = slotNum;
			this.undobuff = buff;
		}
	}
	
	class SlotNumChain{
		int slotNum;
		TableInfo ti;
		
		SlotNumChain(int slotNum, TableInfo ti){
			this.slotNum = slotNum;
			this.ti = ti;
		}
	}
	class ReadsetInfo{
		int slotNum;
		String fldname;
		UndoBuffer undobuff;
		
		ReadsetInfo(int slotNum, String fldname, UndoBuffer buff){
			this.slotNum = slotNum;
			this.undobuff = buff;
			this.fldname = fldname;
		}
	}

	/**
	 * Creates a new transaction and associates it with a recovery manager, a
	 * concurrency manager, and a buffer manager. This constructor depends on
	 * the file, log, and buffer managers from {@link VanillaDb}, which are
	 * created during system initialization. Thus this constructor cannot be
	 * called until {@link VanillaDb#init(String)} is called first.
	 * 
	 * @param txMgr
	 *            the transaction manager
	 * @param concurMgr
	 *            the associated concurrency manager
	 * @param recoveryMgr
	 *            the associated recovery manager
	 * @param bufferMgr
	 *            the associated buffer manager
	 * @param readOnly
	 *            is read-only mode
	 * @param txNum
	 *            the number of the transaction
	 */
	public Transaction(TransactionMgr txMgr, TransactionLifecycleListener concurMgr,
			TransactionLifecycleListener recoveryMgr, TransactionLifecycleListener bufferMgr, boolean readOnly,
			long txNum) {
		this.concurMgr = (ConcurrencyMgr) concurMgr;
		this.recoveryMgr = (RecoveryMgr) recoveryMgr;
		this.bufferMgr = (BufferMgr) bufferMgr;
		this.txNum = txNum;
		this.readOnly = readOnly;
		this.undoBufferMgr = VanillaDb.undoBuffMgr();
		this.startTime = System.nanoTime();
		this.hadDelete = false;

		lifecycleListeners = new LinkedList<TransactionLifecycleListener>();
		// XXX: A transaction manager must be added before a recovery manager to
		// prevent the following scenario:
		// <COMMIT 1>
		// <NQCKPT 1,2>
		//
		// Although, it may create another scenario like this:
		// <NQCKPT 2>
		// <COMMIT 1>
		// But the current algorithm can still recovery correctly during this
		// scenario.
		addLifecycleListener(txMgr);
		/*
		 * A recover manager must be added before a concurrency manager. For
		 * example, if the transaction need to roll back, it must hold all locks
		 * until the recovery procedure complete.
		 */
		addLifecycleListener(recoveryMgr);
		addLifecycleListener(concurMgr);
		addLifecycleListener(bufferMgr);
	}

	public void addLifecycleListener(TransactionLifecycleListener listener) {
		lifecycleListeners.add(listener);
	}

	/**
	 * Commits the current transaction. Flushes all modified blocks (and their
	 * log records), writes and flushes a commit record to the log, releases all
	 * locks, and unpins any pinned blocks.
	 */
	public void commit() {
		if(!readOnly)commitCheckReadSet();
		commitInsertandDelete();
		commitUndoBuffer();
		for (TransactionLifecycleListener l : lifecycleListeners)
			l.onTxCommit(this);

		if (logger.isLoggable(Level.FINE))
			logger.fine("transaction " + txNum + " committed");
	}

	/**
	 * Rolls back the current transaction. Undoes any modified values, flushes
	 * those blocks, writes and flushes a rollback record to the log, releases
	 * all locks, and unpins any pinned blocks.
	 */
	public void rollback() {
		rollBack();
		for (TransactionLifecycleListener l : lifecycleListeners) {

			l.onTxRollback(this);
		}

		if (logger.isLoggable(Level.FINE))
			logger.fine("transaction " + txNum + " rolled back");
	}

	/**
	 * Finishes the current statement. Releases slocks obtained so far for
	 * repeatable read isolation level and does nothing in serializable
	 * isolation level. This method should be called after each SQL statement.
	 */
	public void endStatement() {
		for (TransactionLifecycleListener l : lifecycleListeners)
			l.onTxEndStatement(this);
	}

	public long getTransactionNumber() {
		return this.txNum;
	}

	public boolean isReadOnly() {
		return this.readOnly;
	}

	public RecoveryMgr recoveryMgr() {
		return recoveryMgr;
	}

	public ConcurrencyMgr concurrencyMgr() {
		return concurMgr;
	}

	public BufferMgr bufferMgr() {
		return bufferMgr;
	}
	public boolean hadDelete() {
		return this.hadDelete;
	}
	public void createUndoBuffer(BlockId blk, Constant oldValue, String fldname, int numofSlot,int slotNum)
	{
		UndoBuffer buff = undoBufferMgr.write(blk, txNum,startTime, oldValue, fldname, numofSlot, slotNum);
		UndoBufferInfo buffInfo = new UndoBufferInfo(blk, slotNum, buff);
		undoBufferList.add(buffInfo);
	}
	public Constant readUndoBuffer(BlockId blk, Constant oldValue, String fldname,int slotNum)
	{
		if(readOnly)return undoBufferMgr.readforReadonly(blk, oldValue, startTime, fldname, slotNum);
		else {
			ReadInfo readinfo  = undoBufferMgr.read(blk, oldValue, startTime, fldname, slotNum);
			/*insert into ReadSet and check conflict*/
			ReadsetInfo ri;
			ArrayList<ReadsetInfo> al;
			if(!readSet.containsKey(blk)) {
				ri = new ReadsetInfo(slotNum,fldname,readinfo.getbuff());
				al = new ArrayList<ReadsetInfo>();
				al.add(ri);
				readSet.put(blk, al);
				return readinfo.getvalue();
			}else {
				al = readSet.get(blk);
				Boolean readtwice = false;
				for(ReadsetInfo rinfo : al) {
					if(rinfo.slotNum==slotNum && rinfo.fldname.equals(fldname)) {
						readtwice = true;
						if(rinfo.undobuff==null) {
							if(readinfo.getbuff()!=null)throw new LockAbortException("abort tx." + txNum + " by read write conflict");
						}else {
							if(!rinfo.undobuff.equals(readinfo.getbuff()))throw new LockAbortException("abort tx." + txNum + " by read write conflict");
						}						
					}
				}
				if(!readtwice) {
					ri = new ReadsetInfo(slotNum,fldname,readinfo.getbuff());
					al.add(ri);
					readSet.replace(blk, al);
				}
				return readinfo.getvalue();
				
			}
		}
		
	}
	
	public void checkConflict(BlockId blk, int slotNum) {
		if(!undoBufferMgr.checkOnConflict(blk, slotNum, startTime))
			throw new LockAbortException("abort tx." + txNum + " by write write conflict");
	}
	
	public void InsertMap(BlockId blk, int slotNum, TableInfo ti) {
		if(insertedMap.containsKey(blk)) {
			Vector<SlotNumChain> vec = insertedMap.get(blk);
			SlotNumChain newSlot = new SlotNumChain(slotNum, ti);
			vec.addElement(newSlot);
			insertedMap.replace(blk, vec);
		}else {
			SlotNumChain newSlot = new SlotNumChain(slotNum, ti);
			Vector<SlotNumChain> vec = new Vector<SlotNumChain>();
			vec.addElement(newSlot);
			insertedMap.put(blk,vec);
		}
	}
	public void DeleteMap(BlockId blk, int slotNum, TableInfo ti) {
		if(deleteMap.containsKey(blk)) {
			Vector<SlotNumChain> vec = deleteMap.get(blk);
			SlotNumChain newSlot = new SlotNumChain(slotNum, ti);
			vec.addElement(newSlot);
			deleteMap.replace(blk, vec);
		}else {
			SlotNumChain newSlot = new SlotNumChain(slotNum, ti);
			Vector<SlotNumChain> vec = new Vector<SlotNumChain>();
			vec.addElement(newSlot);
			deleteMap.put(blk,vec);
		}
		this.hadDelete = false;
	}
	
	/** 
	 * prevent insert into the slot previously been inserted by itself
	 * @param blk
	 * @param slotNum
	 * @return
	 */
	public boolean findThisInserted(BlockId blk, int slotNum)
	{
		if(insertedMap.containsKey(blk)) {
			Vector<SlotNumChain> vec = insertedMap.get(blk);
			for(SlotNumChain slotChain : vec) {
				if(slotChain.slotNum == slotNum)
					return true;
			}
		}
		return false;
	}
	public boolean findThisDeleted(BlockId blk, int slotNum)
	{
		if(deleteMap.containsKey(blk)) {
			Vector<SlotNumChain> vec = deleteMap.get(blk);
			for(SlotNumChain slotChain : vec) {
				if(slotChain.slotNum == slotNum)
					return true;
			}
		}
		return false;
	}
	
	public int getInsertSlotNum(BlockId blk, int index)
	{
		if(insertedMap.containsKey(blk))
		{
			Vector<SlotNumChain> vec = insertedMap.get(blk);
			if(vec.size() > index) {
				return vec.get(index).slotNum;
			}
		}
		return -1;
	}
	
	public void checkAndDeleteInsert(BlockId blk, int slotNum)
	{
		if(insertedMap.containsKey(blk)) {
			Vector<SlotNumChain> vec = insertedMap.get(blk);
			for(SlotNumChain slotChain : vec) {
				if(slotChain.slotNum == slotNum) {
					vec.remove(slotChain);
					return;
				}
			}
		}
	}
	/**
	 * Change the inserted records' flag to INUSE before commit
	 */
	
	private void commitInsertandDelete()
	{
		for( Map.Entry<BlockId,Vector<SlotNumChain>> entry : insertedMap.entrySet())
		{
			Vector<SlotNumChain> vec = entry.getValue();
			BlockId blk = entry.getKey();
			RecordPage rp = new RecordPage(blk, vec.get(0).ti, this, true);
			for(SlotNumChain slotChain : vec) {
				rp.moveToId(slotChain.slotNum);
				rp.setValInUse();
			}
		}
		for( Map.Entry<BlockId,Vector<SlotNumChain>> entry : deleteMap.entrySet())
		{
			Vector<SlotNumChain> vec = entry.getValue();
			BlockId blk = entry.getKey();
			RecordPage rp = new RecordPage(blk, vec.get(0).ti, this, true);
			for(SlotNumChain slotChain : vec) {
				rp.moveToId(slotChain.slotNum);
				rp.setValEmpty();
			}
		}
	}
	
	private void commitUndoBuffer()
	{
		long commitTime = System.nanoTime();
		for(UndoBufferInfo buff: undoBufferList)
		{
			//buff.undobuff.getWriteLock();
			buff.undobuff.commit(commitTime);
		}
		/*for(UndoBufferInfo buff: undoBufferList)
		{
			buff.undobuff.releaseWriteLock();
		}*/
		
	}
	private void commitCheckReadSet()
	{
		for( Map.Entry<BlockId,ArrayList<ReadsetInfo>> entry : readSet.entrySet())
		{
			ArrayList<ReadsetInfo> al = entry.getValue();
			BlockId blk = entry.getKey();
			for(ReadsetInfo rin : al) {
				Boolean valid = undoBufferMgr.checkreadset(blk, rin.slotNum, rin.undobuff);
				if(!valid)throw new LockAbortException("abort tx." + txNum + " by read write conflict");
			}
		}
	}
	
	private void rollBack() {
		for(UndoBufferInfo buff : undoBufferList) {
			undoBufferMgr.onTxRollback(buff.blk, buff.slotNum);
		}
		
	}
	
	
	
	
	
}
