package org.vanilladb.core.storage.tx.concurrency;


import org.vanilladb.core.sql.Constant;

public class UndoBuffer {
	private long txNum;
	private long time;
	private boolean isCommitted;
	private String fldname;
	private Constant oldValue;
	private UndoBuffer nextUndoBuffer;
	//private final ReadWriteLock internalLock = new ReentrantReadWriteLock();
	
	public UndoBuffer(long txNum,long time, String fldname, Constant oldValue, UndoBuffer buff)
	{
		this.txNum = txNum;
		this.time = time;
		this.isCommitted = false;
		this.fldname = fldname;
		this.oldValue = oldValue;
		this.nextUndoBuffer = buff;
	}
	public long getTxNum()
	{
		return txNum;
	}
	
	public long getTime()
	{
		return time;
	}
	
	public Constant getVal()
	{
		
		return oldValue;
	}
	
	public void commit(long commitTime)
	{
		this.time = commitTime;
		this.isCommitted = true;
	}
	
	public UndoBuffer next()
	{
		return nextUndoBuffer;
	}
	
	public boolean isCommit()
	{
		return isCommitted;
	}
	
	public String getFldname()
	{
		return fldname;
	}
	
	/*public void getReadLock()
	{
		internalLock.readLock().lock();
	}
	
	public void getWriteLock()
	{
		internalLock.writeLock().lock();
	}
	
	public void releaseReadLock()
	{
		internalLock.readLock().unlock();
	}
	
	public void releaseWriteLock()
	{
		internalLock.writeLock().unlock();
	}*/
	public void setvalue(Constant val)
	{
		oldValue = val;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (obj == null || !(obj.getClass().equals(UndoBuffer.class)))
			return false;
		UndoBuffer buff = (UndoBuffer) obj;
		return fldname.equals(buff.getFldname()) && txNum == buff.getTxNum() && time == buff.getTime() && oldValue.equals(buff.getVal()) && isCommitted == buff.isCommit();
	}
	
}
