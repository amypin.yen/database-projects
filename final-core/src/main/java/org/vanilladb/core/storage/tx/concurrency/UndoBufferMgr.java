package org.vanilladb.core.storage.tx.concurrency;


import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.vanilladb.core.sql.Constant;
import org.vanilladb.core.storage.file.BlockId;

public class UndoBufferMgr {
	private Map<BlockId, Vector<UndoBuffer>> blkmap = new HashMap<BlockId, Vector<UndoBuffer>>();
	
	public UndoBufferMgr()
	{
		
	}
	
	public UndoBuffer write(BlockId blk, long txNum,long txStartTime, Constant oldValue, String fldname, int numofSlot,int slotNum)
	{
		Vector<UndoBuffer> undoVec;
		UndoBuffer undoBuff;
		if(!blkmap.containsKey(blk))
		{
			undoVec = new Vector<UndoBuffer>();
			undoVec.setSize(numofSlot);
			undoBuff = new UndoBuffer(txNum,txStartTime, fldname,oldValue,null);
			undoVec.set(slotNum, undoBuff);
			blkmap.put(blk, undoVec);
			
		}else {
			undoVec = blkmap.get(blk);
			UndoBuffer oldbuff = undoVec.get(slotNum);
			if(oldbuff!=null) {
				if(oldbuff.getTime()>txStartTime) { 
					throw new LockAbortException("abort tx." + txNum + " by write write conflict");
				}
				else if(oldbuff.getTxNum()==txNum && oldbuff.getFldname().equals(fldname)) {
					//oldbuff.setvalue(oldValue);
					undoBuff = oldbuff;
				}
				else {
					undoBuff = new UndoBuffer(txNum,txStartTime, fldname,oldValue,oldbuff);
				}
			}
			else{
				undoBuff = new UndoBuffer(txNum,txStartTime, fldname,oldValue,null);
			}
			undoVec.set(slotNum, undoBuff);
			blkmap.replace(blk, undoVec);
		}

		return undoBuff;
	}
	
	public Constant readforReadonly(BlockId blk, Constant oldValue, long txStartTime, String fldname,int slotNum)
	{
		Vector<UndoBuffer> undoVec;
		if(blkmap.containsKey(blk))
		{
			undoVec = blkmap.get(blk);
			UndoBuffer oldbuff = undoVec.get(slotNum);
			while( oldbuff != null) {
				//oldbuff.getReadLock();
				long bufftx = oldbuff.getTime();
				if (oldbuff.getFldname().equals(fldname)) {
					if(oldbuff.isCommit()) {
						if(bufftx > txStartTime) 
							oldValue = oldbuff.getVal();
						else if(bufftx < txStartTime) {
							//oldbuff.releaseReadLock();
							return oldValue;
						}
					}else if (bufftx == txStartTime) {
						//oldbuff.releaseReadLock();
						return oldValue;
					}else {
						oldValue = oldbuff.getVal();
					}
				}
				//oldbuff.releaseReadLock();
				oldbuff = oldbuff.next();
			}
		}
		return oldValue;	
	}
	public ReadInfo read(BlockId blk, Constant oldValue, long txStartTime, String fldname,int slotNum)
	{
		
		Vector<UndoBuffer> undoVec;
		if(blkmap.containsKey(blk))
		{
			undoVec = blkmap.get(blk);
			UndoBuffer oldbuff = undoVec.get(slotNum);
			while( oldbuff != null) {
				//oldbuff.getReadLock();
				long bufftx = oldbuff.getTime();
				if (oldbuff.getFldname().equals(fldname)) {
					if(oldbuff.isCommit()) {
						if(bufftx > txStartTime) 
							throw new LockAbortException("abort tx. by read write conflict");
						else if(bufftx < txStartTime) {
							//oldbuff.releaseReadLock();
							return new ReadInfo(oldValue,oldbuff);
						}
					}else if (bufftx == txStartTime) {
						//oldbuff.releaseReadLock();
						return new ReadInfo(oldValue,oldbuff);
					}else {
						oldValue = oldbuff.getVal();
					}
				}
				//oldbuff.releaseReadLock();
				oldbuff = oldbuff.next();
			}
		}
		return new ReadInfo(oldValue,null);	
	}
	
	public void onTxRollback(BlockId blk, int slotNum)
	{
		Vector<UndoBuffer> vec = blkmap.get(blk);
		UndoBuffer buff = vec.get(slotNum);
		
		vec.set(slotNum, buff.next());
	}
	
	public boolean checkOnConflict(BlockId blk, int slotNum, long start_time)// And set the chain to null
	{
		if(blkmap.containsKey(blk)) {
			Vector<UndoBuffer> vec = blkmap.get(blk);
			UndoBuffer buff = vec.get(slotNum);
			if(buff != null) {
				if(buff.getTime() <= start_time) {
					vec.set(slotNum, null);
					return true;
				}else {
					return false;
				}
			}
		}
		return true;
	}
	public boolean checkreadset(BlockId blk, int slotNum, UndoBuffer undobuff)
	{
		if(blkmap.containsKey(blk)) {
			Vector<UndoBuffer> vec = blkmap.get(blk);
			UndoBuffer buff = vec.get(slotNum);
			while(buff!=null) {
				if(!buff.isCommit()&&buff.getTxNum() != undobuff.getTxNum()) {
					buff = buff.next();
				}else if(buff.getFldname().equals(undobuff.getFldname())) {
					if(buff.equals(undobuff)) return true;
					else {
						return false;
					}
				}else {
					buff = buff.next();
				}	
			}
			if(undobuff!=null) {
				return false;
			}
		}
		return true;
	}
}
