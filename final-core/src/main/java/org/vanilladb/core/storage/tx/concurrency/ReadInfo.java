package org.vanilladb.core.storage.tx.concurrency;
import org.vanilladb.core.sql.Constant;
public class ReadInfo {
	private Constant value;
	private UndoBuffer buff;
	
	public ReadInfo(Constant value,UndoBuffer buff)
	{
		this.value = value;
		this.buff = buff;
	}
	public Constant getvalue()
	{
		return this.value;
	}
	public UndoBuffer getbuff()
	{
		return this.buff;
	}
}
